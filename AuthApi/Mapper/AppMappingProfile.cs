﻿using AuthApi.DBEntities;
using AuthApi.DTO;
using AutoMapper;
using Microsoft.Extensions.Hosting;

namespace AuthApi.Mapper
{
    public class AppMappingProfile : Profile
    {
        public AppMappingProfile()
        {
            CreateMap<User, RegistrationDTO>().ReverseMap();
        }
    }
}
