﻿using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using AuthApi.DTO;
using AuthApi.Services.Interfaces;

namespace AuthApi.Controllers
{
    [Route("api")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("register")]
        [SwaggerOperation
            (
                Description = "Регистрация нового пользователя",
                Summary = "Регистрация"
            )
         ]
        public async Task<IActionResult> Register([FromBody] RegistrationDTO registration)
        {
            bool result = await _userService.Register(registration);
            return Ok(result);
        }

        [HttpPost("login")]
        [SwaggerOperation
            (
                Description = "Логин через получение JWT Bearer токена для доступа к системе",
                Summary = "Логин"
            )
         ]
        public async Task<IActionResult> Login([FromBody] LoginDTO login)
        {
            TokenAnswer tokenAnswer = await _userService.Login(login);

            return Ok(tokenAnswer);
        }
    }
}
