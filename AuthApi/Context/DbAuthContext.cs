﻿using AuthApi.DBEntities;
using Microsoft.EntityFrameworkCore;

namespace AuthApi.Context
{
    public class DbAuthContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbAuthContext(DbContextOptions<DbAuthContext> options) : base(options)
        {

        }
    }
}
