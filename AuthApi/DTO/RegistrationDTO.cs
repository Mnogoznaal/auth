﻿namespace AuthApi.DTO
{
    public class RegistrationDTO
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
