﻿namespace AuthApi.DTO
{
    public class TokenAnswer
    {
        public bool IsValid { get; set; }
        public string AccessToken { get; set; }
        public string Username { get; set; }
    }
}
