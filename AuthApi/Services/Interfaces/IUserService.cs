﻿using AuthApi.DTO;

namespace AuthApi.Services.Interfaces
{
    public interface IUserService
    {
        Task<TokenAnswer> Login(LoginDTO loginDTO);
        Task<bool> Register(RegistrationDTO loginDTO);
        bool IsTokenValid(string token);
    }
}
