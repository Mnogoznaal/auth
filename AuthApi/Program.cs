using AuthApi.Context;
using AuthApi.Mapper;
using AuthApi.Repositories;
using AuthApi.Repositories.Interfaces;
using AuthApi.Services;
using AuthApi.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using AuthApi.Background;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

string con = builder.Configuration.GetConnectionString("DefaultConnection");
builder.Services.AddDbContext<DbAuthContext>(options => options.UseNpgsql(con));
builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<IUserRepository, UserRepository>();
builder.Services.AddAutoMapper(typeof(AppMappingProfile));
builder.Services.AddHostedService<RabbitBackgroundService>();
AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();