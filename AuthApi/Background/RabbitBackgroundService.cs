﻿using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using System.Text;
using AuthApi.Services.Interfaces;

namespace AuthApi.Background
{
    public class RabbitBackgroundService : BackgroundService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IConfiguration _configuration;
        public RabbitBackgroundService(IServiceProvider serviceProvider, IConfiguration configuration)
        {
            _serviceProvider = serviceProvider;
            _configuration = configuration;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                var factory = new ConnectionFactory { HostName = _configuration.GetConnectionString("RabbitHost"), Port = Convert.ToInt32(_configuration.GetConnectionString("RabbitPort")), UserName = _configuration.GetConnectionString("RabbitUserName"), Password = _configuration.GetConnectionString("RabbitPassword") };
                using var connection = factory.CreateConnection();
                using var channel = connection.CreateModel();

                channel.QueueDeclare(queue: "rpc_queue",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);
                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                channel.BasicConsume(queue: "rpc_queue",
                                     autoAck: false,
                                     consumer: consumer);
                Console.WriteLine(" [x] Awaiting RPC requests");

                consumer.Received += (model, ea) =>
                {
                    bool response = false;

                    var body = ea.Body.ToArray();
                    var props = ea.BasicProperties;
                    var replyProps = channel.CreateBasicProperties();
                    replyProps.CorrelationId = props.CorrelationId;

                    try
                    {
                        var message = Encoding.UTF8.GetString(body);
                        Console.WriteLine($"GotMessage ({message})");
                        using (var scope = _serviceProvider.CreateScope())
                        {
                            var userService = scope.ServiceProvider.GetRequiredService<IUserService>();
                            response = userService.IsTokenValid(message);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"{e.Message}");
                    }
                    finally
                    {
                        var responseBytes = Encoding.UTF8.GetBytes(response.ToString());
                        channel.BasicPublish(exchange: string.Empty,
                                             routingKey: props.ReplyTo,
                                             basicProperties: replyProps,
                                             body: responseBytes);
                        channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                    }
                };

                await Task.Delay(TimeSpan.FromSeconds(10), stoppingToken); // Adjust the delay as needed.
            }
        }
    }
}
