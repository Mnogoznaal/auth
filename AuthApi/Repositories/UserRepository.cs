﻿using AuthApi.Context;
using AuthApi.DBEntities;
using AuthApi.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace AuthApi.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly DbAuthContext _dbAuthContext;
        public UserRepository(DbAuthContext dbAuthContext)
        {
            _dbAuthContext = dbAuthContext;
        }

        public async Task Create(User user)
        {
            await _dbAuthContext.Users.AddAsync(user);
            await _dbAuthContext.SaveChangesAsync();
        }

        public async Task<User> GetById(int id)
        {
            return await _dbAuthContext.Users.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<User> GetByLogin(string login)
        {
            return await _dbAuthContext.Users.FirstOrDefaultAsync(x => x.Login == login);
        }
    }
}
