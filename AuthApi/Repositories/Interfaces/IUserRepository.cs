﻿using AuthApi.DBEntities;

namespace AuthApi.Repositories.Interfaces
{
    public interface IUserRepository
    {
        Task Create(User user);
        Task<User> GetById(int id);
        Task<User> GetByLogin(string login);
    }
}
