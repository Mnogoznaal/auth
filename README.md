# Auth

Сервис отвечающий за регистрацию и вход пользователей.
Основной сервис для всех остальных.
Для остальных сервисов проверяет токен.

## Настройка

В appsettings.json изменить строки подключений
1. База данных Postgres
2. Брокер RabbitMQ

Рэбит я поднимал в докере
docker run -d --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management